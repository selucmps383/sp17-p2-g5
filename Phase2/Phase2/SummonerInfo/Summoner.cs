using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Linq;

namespace Phase2
{
    public class Summoner
    {
        public long id { get; set; }
        public string name { get; set; }
        public int profileIconId { get; set; }
        public long revisionDate { get; set; }
        public long summonerLevel { get; set; }

        public static Summoner getSummoner(string summonerName)
        {
            Summoner summoner = new Summoner();

            var client = new RestClient("https://na.api.pvp.net");
            RestRequest request = new RestRequest("/api/lol/na/v1.4/summoner/by-name/" + summonerName + "?api_key=RGAPI-96a56101-1da0-43fe-94bf-289722d609f3", Method.GET);
            request.AddHeader("Content-type", "application/json");

            var response = client.Execute<Summoner>(request);
            var data = JsonConvert.DeserializeObject<Dictionary<string, Summoner>>(response.Content);

            summoner = data.First().Value;
            summoner.id = data.First().Value.id;
            summoner.name = data.First().Value.name;
            summoner.profileIconId = data.First().Value.profileIconId;
            summoner.revisionDate = data.First().Value.revisionDate;
            summoner.summonerLevel = data.First().Value.summonerLevel;

            return summoner;
        }
    }

    public class RootObject
    {
        public Summoner summoner { get; set; }
    }
}
