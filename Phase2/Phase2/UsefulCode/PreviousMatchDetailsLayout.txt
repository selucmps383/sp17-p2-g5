﻿<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:p1="http://schemas.android.com/apk/res/android"
    p1:orientation="vertical"
    p1:layout_width="match_parent"
    p1:layout_height="match_parent"
    p1:id="@+id/linearLayout1">
    <TextView
        p1:text="Game Stats Go Here"
        p1:textAppearance="?android:attr/textAppearanceLarge"
        p1:layout_width="match_parent"
        p1:layout_height="wrap_content"
        p1:id="@+id/textView1" />
    <LinearLayout
        p1:id="@+id/linearLayout1"
        p1:layout_width="match_parent"
        p1:layout_height="wrap_content"
        p1:weightSum="10">
        <LinearLayout
            p1:orientation="vertical"
            p1:layout_width="fill_parent"
            p1:layout_height="match_parent"
            p1:layout_weight="5"
            p1:id="@+id/linearLayout2">
            <ImageView
                p1:src="@android:drawable/ic_menu_gallery"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/imageView1" />
            <TextView
                p1:text="User"
                p1:textAppearance="?android:attr/textAppearanceLarge"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView1" />
            <TextView
                p1:text="User's Score"
                p1:textAppearance="?android:attr/textAppearanceMedium"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView3" />
            <GridLayout
                p1:layout_width="match_parent"
                p1:layout_height="35.5dp"
                p1:id="@+id/gridLayout1">
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView2" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView3" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView4" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView5" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView6" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView7" />
            </GridLayout>
            <ImageView
                p1:src="@android:drawable/ic_menu_gallery"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/imageView1" />
            <TextView
                p1:text="Teammate 1"
                p1:textAppearance="?android:attr/textAppearanceLarge"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView1" />
            <TextView
                p1:text="Teammate 1's Score"
                p1:textAppearance="?android:attr/textAppearanceMedium"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView3" />
            <GridLayout
                p1:layout_width="match_parent"
                p1:layout_height="35.5dp"
                p1:id="@+id/gridLayout1">
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView2" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView3" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView4" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView5" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView6" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView7" />
            </GridLayout>
            <ImageView
                p1:src="@android:drawable/ic_menu_gallery"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/imageView1" />
            <TextView
                p1:text="Teammate 2"
                p1:textAppearance="?android:attr/textAppearanceLarge"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView1" />
            <TextView
                p1:text="Teammate 2's Score"
                p1:textAppearance="?android:attr/textAppearanceMedium"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView3" />
            <GridLayout
                p1:layout_width="match_parent"
                p1:layout_height="35.5dp"
                p1:id="@+id/gridLayout1">
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView2" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView3" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView4" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView5" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView6" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView7" />
            </GridLayout>
            <ImageView
                p1:src="@android:drawable/ic_menu_gallery"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/imageView1" />
            <TextView
                p1:text="Large Text"
                p1:textAppearance="?android:attr/textAppearanceLarge"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView1" />
            <TextView
                p1:text="Medium Text"
                p1:textAppearance="?android:attr/textAppearanceMedium"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView3" />
            <GridLayout
                p1:layout_width="match_parent"
                p1:layout_height="35.5dp"
                p1:id="@+id/gridLayout1">
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView2" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView3" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView4" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView5" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView6" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView7" />
            </GridLayout>
            <ImageView
                p1:src="@android:drawable/ic_menu_gallery"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/imageView1" />
            <TextView
                p1:text="Large Text"
                p1:textAppearance="?android:attr/textAppearanceLarge"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView1" />
            <TextView
                p1:text="Medium Text"
                p1:textAppearance="?android:attr/textAppearanceMedium"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView3" />
            <GridLayout
                p1:layout_width="match_parent"
                p1:layout_height="35.5dp"
                p1:id="@+id/gridLayout1">
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView2" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView3" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView4" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView5" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView6" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView7" />
            </GridLayout>
        </LinearLayout>
        <LinearLayout
            p1:orientation="vertical"
            p1:layout_width="fill_parent"
            p1:layout_height="match_parent"
            p1:layout_weight="5"
            p1:id="@+id/linearLayout2">
            <ImageView
                p1:src="@android:drawable/ic_menu_gallery"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/imageView1" />
            <TextView
                p1:text="Opponent 1"
                p1:textAppearance="?android:attr/textAppearanceLarge"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView1" />
            <TextView
                p1:text="Opponent 1's Score"
                p1:textAppearance="?android:attr/textAppearanceMedium"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView3" />
            <GridLayout
                p1:layout_width="match_parent"
                p1:layout_height="35.5dp"
                p1:id="@+id/gridLayout1">
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView2" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView3" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView4" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView5" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView6" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView7" />
            </GridLayout>
            <ImageView
                p1:src="@android:drawable/ic_menu_gallery"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/imageView1" />
            <TextView
                p1:text="Large Text"
                p1:textAppearance="?android:attr/textAppearanceLarge"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView1" />
            <TextView
                p1:text="Medium Text"
                p1:textAppearance="?android:attr/textAppearanceMedium"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView3" />
            <GridLayout
                p1:layout_width="match_parent"
                p1:layout_height="35.5dp"
                p1:id="@+id/gridLayout1">
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView2" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView3" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView4" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView5" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView6" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView7" />
            </GridLayout>
            <ImageView
                p1:src="@android:drawable/ic_menu_gallery"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/imageView1" />
            <TextView
                p1:text="Large Text"
                p1:textAppearance="?android:attr/textAppearanceLarge"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView1" />
            <TextView
                p1:text="Medium Text"
                p1:textAppearance="?android:attr/textAppearanceMedium"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView3" />
            <GridLayout
                p1:layout_width="match_parent"
                p1:layout_height="35.5dp"
                p1:id="@+id/gridLayout1">
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView2" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView3" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView4" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView5" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView6" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView7" />
            </GridLayout>
            <ImageView
                p1:src="@android:drawable/ic_menu_gallery"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/imageView1" />
            <TextView
                p1:text="Large Text"
                p1:textAppearance="?android:attr/textAppearanceLarge"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView1" />
            <TextView
                p1:text="Medium Text"
                p1:textAppearance="?android:attr/textAppearanceMedium"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView3" />
            <GridLayout
                p1:layout_width="match_parent"
                p1:layout_height="35.5dp"
                p1:id="@+id/gridLayout1">
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView2" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView3" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView4" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView5" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView6" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView7" />
            </GridLayout>
            <ImageView
                p1:src="@android:drawable/ic_menu_gallery"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/imageView1" />
            <TextView
                p1:text="Large Text"
                p1:textAppearance="?android:attr/textAppearanceLarge"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView1" />
            <TextView
                p1:text="Medium Text"
                p1:textAppearance="?android:attr/textAppearanceMedium"
                p1:layout_width="match_parent"
                p1:layout_height="wrap_content"
                p1:id="@+id/textView3" />
            <GridLayout
                p1:layout_width="match_parent"
                p1:layout_height="35.5dp"
                p1:id="@+id/gridLayout1">
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView2" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView3" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView4" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView5" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView6" />
                <ImageView
                    p1:src="@android:drawable/ic_menu_gallery"
                    p1:id="@+id/imageView7" />
            </GridLayout>
        </LinearLayout>
    </LinearLayout>
</LinearLayout>