namespace Phase2
{
    public class Stats
    {
        public int totalDamageDealtToChampions { get; set; }
        public int goldEarned { get; set; }
        public int item0 { get; set; }
        public int assists { get; set; }
        public int totalDamageTaken { get; set; }
        public int magicDamageTaken { get; set; }
        public int physicalDamageDealtPlayer { get; set; }
        public int numDeaths { get; set; }
        public int physicalDamageTaken { get; set; }
        public int team { get; set; }
        public bool win { get; set; }
        public int totalDamageDealt { get; set; }
        public int totalUnitsHealed { get; set; }
        public int totalHeal { get; set; }
        public int level { get; set; }
        public int minionsKilled { get; set; }
        public int timePlayed { get; set; }
        public int physicalDamageDealtToChampions { get; set; }
        public int goldSpent { get; set; }
        public int magicDamageDealtToChampions { get; set; }
        public int magicDamageDealtPlayer { get; set; }
    }
}