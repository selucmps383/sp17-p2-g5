using System.Collections.Generic;
using Newtonsoft.Json;
using RestSharp;

namespace Phase2
{
    public class RecentGames
    {
        [JsonProperty("games")]
        public List<CurrentGame> games { get; set; }
        [JsonProperty("summonerId")]
        public int summonerId { get; set; }

        public static List<CurrentGame> getRecentGames(long summonerId)
        {
            var client = new RestClient("https://na.api.pvp.net");
            var request = new RestRequest("/api/lol/na/v1.3/game/by-summoner/" + summonerId + "/recent?api_key=RGAPI-96a56101-1da0-43fe-94bf-289722d609f3", Method.GET);
            request.AddHeader("Content-type", "application/json");
            var response = client.Execute(request);




            return JsonConvert.DeserializeObject<RecentGames>(response.Content).games;
        }
    }


}