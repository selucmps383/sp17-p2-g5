using System.Collections.Generic;

namespace Phase2
{
    public class CurrentGame
    {
        public List<FellowPlayer> fellowPlayers { get; set; }
        public string gameType { get; set; }
        public Stats stats { get; set; }
        public long gameId { get; set; }
        public int ipEarned { get; set; }
        public int spell1 { get; set; }
        public int teamId { get; set; }
        public int spell2 { get; set; }
        public string gameMode { get; set; }
        public int mapId { get; set; }
        public int level { get; set; }
        public bool invalid { get; set; }
        public string subType { get; set; }
        public long createDate { get; set; }
        public int championId { get; set; }
    }
}


