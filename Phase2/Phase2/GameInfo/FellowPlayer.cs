namespace Phase2
{
    public class FellowPlayer
    {
        public int championId { get; set; }
        public int teamId { get; set; }
        public int summonerId { get; set; }
    }
}