using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Views;
using Android.Widget;
using RiotSharp.GameEndpoint;
using RiotSharp;
using Android.Graphics;
using System.Net;

namespace Phase2
{
    public class MatchHistoryAdapter : BaseAdapter<Game>
    {
        List<Game> recentGames;
        Context adapterContext;

        public MatchHistoryAdapter(Context context, List<Game> recentGames)
        {
            this.recentGames = recentGames;
            adapterContext = context;
        }
        public override int Count
        {
            get
            {
                return recentGames.Count();
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Game this[int position]
        {
            get { return recentGames[position]; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            View row = convertView;

            if (row == null)
            {
                row = LayoutInflater.From(adapterContext).Inflate(Resource.Layout.MatchHistory, null, false);
            }
            LinearLayout rowLayout = row.FindViewById<LinearLayout>(Resource.Id.linearLayout1);
            TextView matchText = row.FindViewById<TextView>(Resource.Id.matchHistoryTextView);
            ImageView championImageView = row.FindViewById<ImageView>(Resource.Id.championImage);

            var apiKey = "RGAPI-96a56101-1da0-43fe-94bf-289722d609f3";
            var api = RiotApi.GetInstance(apiKey, 10, 10);
            var staticApi = StaticRiotApi.GetInstance(apiKey);

            var gameMode = recentGames[position].GameMode;
            var gameType = recentGames[position].GameType;
            var championId = recentGames[position].ChampionId;
            var win = recentGames[position].Statistics.Win;
            var KDA = "" + recentGames[position].Statistics.ChampionsKilled + "/" + recentGames[position].Statistics.NumDeaths + "/" + recentGames[position].Statistics.Assists;
            var champion = staticApi.GetChampion(RiotSharp.Region.na, championId);
            var gameId = recentGames[position].GameId;

            var combatScore = recentGames[position].Statistics.CombatPlayerScore;
            var goldEarned = recentGames[position].Statistics.GoldEarned;
            var totalDamageDealt = recentGames[position].Statistics.TotalDamageDealt;
            var totalDamageDealtToChampions = recentGames[position].Statistics.TotalDamageDealtToChampions;
            var totalDamageTaken = recentGames[position].Statistics.TotalDamageTaken;
            var championsKilled = recentGames[position].Statistics.ChampionsKilled;
            var barracksKilled = recentGames[position].Statistics.BarracksKilled;
            var minionsKilled = recentGames[position].Statistics.MinionsKilled;
            var turretsKilled = recentGames[position].Statistics.TurretsKilled;
            var itemOne = recentGames[position].Statistics.Item0;
            var itemTwo = recentGames[position].Statistics.Item1;
            var itemThree = recentGames[position].Statistics.Item2;
            var itemFour = recentGames[position].Statistics.Item3;
            var itemFive = recentGames[position].Statistics.Item4;
            var itemSix = recentGames[position].Statistics.Item5;
            var itemSeven = recentGames[position].Statistics.Item6;

            if (win)
            {
                rowLayout.SetBackgroundColor(Color.Rgb(93, 188, 210));
            }
            else
            {
                rowLayout.SetBackgroundColor(Color.Rgb(148, 65, 71));
            }

            matchText.Text = (win ? "Victory" : "Defeat") + "\nGame Mode: " + gameMode + "\nKDA:  " + KDA + "\nChampion: " + champion.Name;
            matchText.SetTextColor(Color.White);

            var imageBitmap = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/champion/" + champion.Key + ".png");
            championImageView.SetImageBitmap(imageBitmap);
            string championKey = champion.Key;

            rowLayout.Click += delegate
            {
                sendGameStats(win, combatScore, goldEarned, totalDamageDealt, totalDamageTaken, championsKilled, minionsKilled, turretsKilled, championKey, itemOne, itemTwo, itemThree, itemFour, itemFive, itemSix, itemSeven, adapterContext);
            };

            return row;

        }
        public static void sendGameStats(Boolean win, int combatScore, int goldEarned, int totalDamageDealt, int totalDamageTaken, int championsKilled, int minionsKilled, int turretsKilled, string championKey, int itemOne, int itemTwo, int itemThree, int itemFour, int itemFive, int itemSix, int itemSeven, Context context)
        {
            var matchDetails = new Intent(context, typeof(MatchDetailActivity));

            matchDetails.PutExtra("win", win);
            matchDetails.PutExtra("combatScore", combatScore);
            matchDetails.PutExtra("goldEarned", goldEarned);
            matchDetails.PutExtra("totalDamageDealt", totalDamageDealt);
            matchDetails.PutExtra("totalDamageTaken", totalDamageTaken);
            matchDetails.PutExtra("championsKilled", championsKilled);
            matchDetails.PutExtra("minionsKilled", minionsKilled);
            matchDetails.PutExtra("turretsKilled", turretsKilled);
            matchDetails.PutExtra("championKey", championKey);
            matchDetails.PutExtra("itemOne", itemOne);
            matchDetails.PutExtra("itemTwo", itemTwo);
            matchDetails.PutExtra("itemThree", itemThree);
            matchDetails.PutExtra("itemFour", itemFour);
            matchDetails.PutExtra("itemFive", itemFive);
            matchDetails.PutExtra("itemSix", itemSix);
            matchDetails.PutExtra("itemSeven", itemSeven);

            context.StartActivity(matchDetails);
        }

        public static Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;
            if (!(url == "null"))
                using (var webClient = new WebClient())
                {
                    var imageBytes = webClient.DownloadData(url);
                    if (imageBytes != null && imageBytes.Length > 0)
                    {
                        imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                    }
                }

            return imageBitmap;
        }
    }
}