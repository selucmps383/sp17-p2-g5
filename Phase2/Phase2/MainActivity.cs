﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.OS;
using Android.Widget;
using RestSharp;
using Newtonsoft.Json;
using Android.Content;
using System.Text.RegularExpressions;

namespace Phase2
{
    [Activity(Label = "Phase2", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        ProgressDialog progress;

        string summonerBeingCalled;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            Button button = FindViewById<Button>(Resource.Id.getSummonerButton);
            EditText userInput = FindViewById<EditText>(Resource.Id.summonerNameEditText);
            TextView display = FindViewById<TextView>(Resource.Id.textView1);

            //moved this from below to use Text in IsMatch method
            summonerBeingCalled = userInput.Text;

            //tests if text matches regex condition, does all below, if not throws exception
            Regex testValidSummoner = new Regex(@"^[0-9\\p{L} _\\.]+$");

            if (testValidSummoner.IsMatch(summonerBeingCalled))//if Regex conditions met
            {
                button.Click += delegate
                {
                    progress = new ProgressDialog(this);
                    progress.SetMessage("Please wait...");
                    progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                    progress.Show();

                    var client = new RestClient("https://na.api.pvp.net");
                    RestRequest request = new RestRequest("/api/lol/na/v1.4/summoner/by-name/" + summonerBeingCalled + "?api_key=RGAPI-96a56101-1da0-43fe-94bf-289722d609f3", Method.GET);
                    request.AddHeader("Content-type", "application/json");

                    var response = client.Execute<Summoner>(request);
                    var data = JsonConvert.DeserializeObject<Dictionary<string, Summoner>>(response.Content);

                    //assign values to summoner object
                    var summoner = data.First().Value;
                    summoner.id = data.First().Value.id;
                    summoner.name = data.First().Value.name;
                    summoner.profileIconId = data.First().Value.profileIconId;
                    summoner.revisionDate = data.First().Value.revisionDate;
                    summoner.summonerLevel = data.First().Value.summonerLevel;

                    display.Text = ("" + summoner.id);

                    RunOnUiThread(delegate
                    {
                        progress.Dismiss();
                        var content = response.Content;
                        Toast.MakeText(this, data.First().Value.name, ToastLength.Long).Show();
                    });

                    var matchHistory = new Intent(this, typeof(MatchHistoryActivity));
                    matchHistory.PutExtra("MyData", summoner.id);
                    StartActivity(matchHistory);
                };
            }
            else //if Regex conditions are not met
            {
                Toast.MakeText(this, "Please enter a valid Summoner name", ToastLength.Short).Show();
            }
        }
    }
}


