﻿using Android.App;
using Android.OS;
using Android.Widget;
using Android.Content;
using RiotSharp;
using System;

namespace Phase2
{
    [Activity(Label = "Phase2", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            Button button = FindViewById<Button>(Resource.Id.getSummonerButton);
            EditText userInput = FindViewById<EditText>(Resource.Id.summonerNameEditText);

            var apiKey = "RGAPI-96a56101-1da0-43fe-94bf-289722d609f3";
            var api = RiotApi.GetInstance(apiKey, 10, 500);

            button.Click += delegate
            {
                try
                {
                    var summoner = api.GetSummoner(Region.na, userInput.Text);
                    long summonerId = summoner.Id;
                    string summonerName = summoner.Name;

                    var matchHistory = new Intent(this, typeof(MatchHistoryActivity));
                    matchHistory.PutExtra("summonerId", summonerId);
                    matchHistory.PutExtra("summonerName", summonerName);

                    StartActivity(matchHistory);
                }
                catch (Exception e)
                {
                    Toast.MakeText(this, "Please enter an existing summoner", ToastLength.Long).Show();
                }
            };
        }
    }
}


