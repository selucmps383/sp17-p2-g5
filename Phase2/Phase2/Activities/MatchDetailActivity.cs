﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using RiotSharp;
using System;

namespace Phase2
{
    [Activity(Label = "MatchDetail")]
    public class MatchDetailActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MatchDetail);

            ImageView playerChampionImageView = FindViewById<ImageView>(Resource.Id.playerChampionImageView);
            TextView gameResultTextView = FindViewById<TextView>(Resource.Id.gameResultTextView);
            TextView gameStatsTextView = FindViewById<TextView>(Resource.Id.gameStatsTextView);
            ImageView itemOneView = FindViewById<ImageView>(Resource.Id.imageView1);
            ImageView itemTwoView = FindViewById<ImageView>(Resource.Id.imageView2);
            ImageView itemThreeView = FindViewById<ImageView>(Resource.Id.imageView3);
            ImageView itemFourView = FindViewById<ImageView>(Resource.Id.imageView4);
            ImageView itemFiveView = FindViewById<ImageView>(Resource.Id.imageView5);
            ImageView itemSixView = FindViewById<ImageView>(Resource.Id.imageView6);
            ImageView itemSevenView = FindViewById<ImageView>(Resource.Id.imageView7);

            var apiKey = "RGAPI-96a56101-1da0-43fe-94bf-289722d609f3";
            var api = RiotApi.GetInstance(apiKey, 10, 500);

            var championKey = Intent.GetStringExtra("championKey");
            Boolean win = Intent.GetBooleanExtra("win", false);
            var goldEarned = Intent.GetIntExtra("goldEarned", 999);
            var totalDamageDealt = Intent.GetIntExtra("totalDamageDealt", 999);
            var totalDamageTaken = Intent.GetIntExtra("totalDamageTaken", 999);
            var championsKilled = Intent.GetIntExtra("championsKilled", 999);
            var minionsKilled = Intent.GetIntExtra("minionsKilled", 999);
            var itemOne = Intent.GetIntExtra("itemOne", 999);
            var itemTwo = Intent.GetIntExtra("itemTwo", 999);
            var itemThree = Intent.GetIntExtra("itemThree", 999);
            var itemFour = Intent.GetIntExtra("itemFour", 999);
            var itemFive = Intent.GetIntExtra("itemFive", 999);
            var itemSix = Intent.GetIntExtra("itemSix", 999);
            var itemSeven = Intent.GetIntExtra("itemSeven", 999);


            string goldEarnedFormatted = goldEarned.ToString("N0");
            string totalDamageDealtFormatted = totalDamageDealt.ToString("N0");
            string totalDamageTakenFormatted = totalDamageTaken.ToString("N0");
            string championsKilledFormatted = championsKilled.ToString("N0");
            string minionsKilledFormatted = minionsKilled.ToString("N0");
            
            var imageBitmap = MatchHistoryAdapter.GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/champion/" + championKey + ".png");
            playerChampionImageView.SetImageBitmap(imageBitmap);
            if(itemOne!=0)itemOneView.SetImageBitmap(MatchHistoryAdapter.GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + itemOne + ".png"));
            if (itemTwo != 0) itemTwoView.SetImageBitmap(MatchHistoryAdapter.GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + itemTwo + ".png"));
            if (itemThree != 0) itemThreeView.SetImageBitmap(MatchHistoryAdapter.GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + itemThree + ".png"));
            if (itemFour != 0) itemFourView.SetImageBitmap(MatchHistoryAdapter.GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + itemFour + ".png"));
            if (itemFive != 0) itemFiveView.SetImageBitmap(MatchHistoryAdapter.GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + itemFive + ".png"));
            if (itemSix != 0) itemSixView.SetImageBitmap(MatchHistoryAdapter.GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + itemSix + ".png"));
            if (itemSeven != 0) itemSevenView.SetImageBitmap(MatchHistoryAdapter.GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + itemSeven + ".png"));
            gameResultTextView.Text = (win ? "VICTORY" : "DEFEAT");
            if (win)
            {
                gameResultTextView.SetTextColor(Color.SteelBlue);
            }
            else
            {
                gameResultTextView.SetTextColor(Color.DarkRed);

            }
            gameResultTextView.TextSize = 50;

            gameStatsTextView.Text = "\nChampions Killed: " + championsKilledFormatted + "\nMinions Killed: " + minionsKilledFormatted + "\nDamage Dealt: " + totalDamageDealtFormatted + "\nDamage Taken: " + totalDamageTakenFormatted + "\nGold Earned: " + goldEarnedFormatted;

        }
    }
}