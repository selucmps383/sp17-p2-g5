using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using RiotSharp;
using System;


namespace Phase2
{
    [Activity(Label = "MatchHistory")]
    public class MatchHistoryActivity : ListActivity
    {
        public string summonerName;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var apiKey = "RGAPI-96a56101-1da0-43fe-94bf-289722d609f3";
            var api = RiotApi.GetInstance(apiKey, 10, 10);

            summonerName = Intent.GetStringExtra("summonerName");
            var summoner = api.GetSummoner(Region.na, summonerName);
            Window.SetTitle(summonerName);
            try
            {
                var recentGames = api.GetRecentGames(Region.na, summoner.Id);
                ListAdapter = new MatchHistoryAdapter(this, recentGames);
            }
            catch (Exception e)
            {
                Toast.MakeText(this, "Error, enter a different user", ToastLength.Long).Show();
                var mainActivity = new Intent(this, typeof(MainActivity));
                StartActivity(mainActivity);
            }
        }

        public override void OnAttachedToWindow()
        {
            Window.SetTitle(summonerName);
        }

    }
}