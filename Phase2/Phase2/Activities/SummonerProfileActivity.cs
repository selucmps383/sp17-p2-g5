using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using RiotSharp;

namespace Phase2.Activities
{
    [Activity(Label = "SummonerProfileActivity")]
    public class SummonerProfileActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var apiKey = "RGAPI-96a56101-1da0-43fe-94bf-289722d609f3";
            var api = RiotApi.GetInstance(apiKey,10,500);
            ImageView playerChampionImage = FindViewById<ImageView>(Resource.Id.playerChampionImageView);

            var summonerName = Intent.GetStringExtra("summonerName");
            var summoner = api.GetSummoner(Region.na, summonerName);

            var championKey = Intent.GetStringExtra("championKey");

            var imageBitmap = MatchHistoryAdapter.GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/champion/" + championKey + ".png");
            playerChampionImage.SetImageBitmap(imageBitmap);

        }
    }
}